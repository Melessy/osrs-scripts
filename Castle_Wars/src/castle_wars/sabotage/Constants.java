package castle_wars.sabotage;

import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.api.wrappers.items.Item;
/**
 *
 * @author t7emon
 */
public class Constants 
{
    /**
     * Variables
     */
        public static GameObject guthixPortal;
        public static GameObject energyBarrier;
        public static GameObject Ladder;
        public static NPC Barricade;
        public static Item barricadeInventory;
        public static int gameCount = 0;
}

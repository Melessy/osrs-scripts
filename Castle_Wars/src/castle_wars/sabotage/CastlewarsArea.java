package castle_wars.sabotage;

import org.dreambot.api.methods.map.Area;

/**
 *
 * @author t7emon
 */
public class CastlewarsArea {
    
    /**
    * The area enum
    */
    public enum Areas {
      castlewarsLobby(new Area(2435, 3098, 2446, 3080, 0)),
      
      zamorakWaitingRoom(new Area(2429, 9515, 2412, 9518, 0)),
      saradominWaitingRoom(new Area(2391, 9483, 2370, 9491, 0)),
      
      saradominRespawnRoom(new Area(2423, 3080, 2431, 3072, 1)),
      saradominFirstFloor(new Area(2420, 3072, 2422, 3083, 1)),
      saradominMainFloor(new Area(2431, 3072, 2417, 3086, 0)),
      
      zamorakRespawnRoom(new Area(2368, 3135, 2376, 3127, 1)),
      zamorakFirstFloor(new Area(2377, 3135, 2379, 3124, 1)),
      zamorakMainFloor(new Area(2368, 3135, 2382, 3121, 0));
      
      /**
      * area variable
      */
     private Area area;
     
     /**
     * The area setter.
     */
      private Areas(Area area) {
          this.area = area;
      }
      /**
      * The area getter.
      */
      public Area area() {
          return area;
      }
    }
    
}

package castle_wars.sabotage;


import castle_wars.sabotage.CastlewarsArea.Areas;
import static castle_wars.sabotage.Constants.*;
import java.awt.Color;
import java.awt.Graphics;
import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.map.Area;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.utilities.Timer;
import org.dreambot.api.wrappers.widgets.WidgetChild;
import org.dreambot.api.wrappers.widgets.message.Message;

/**
 *
 * @author t7emon
 */
@ScriptManifest(
        author = "T7emon", 
        name = "Castle_Wars_Sabotager", 
        version = 1.0, 
        description = "Sabotage Castle Wars Games", 
        category = Category.MINIGAME)

public class Main extends AbstractScript 
{
    private Timer timer;
    private boolean saradominTeam = false;
    private boolean zamorakTeam = false;
    
        /*
         * Is in area? wether true or false
         */
    private boolean inArea(Area area)
    { 
    if(area.contains(getLocalPlayer().getTile()))
    {
        return true;
    }
    return false;
}

    @Override
	public void onStart() 
        {
                timer = new Timer();
                log("Initialized");
		log("Welcome to Castle Wars Sabotager by T7emon.");
        }
        
      @Override
      public void onMessage(Message msg) 
      {
	if (msg.getMessage().contains("")) 
        {
           gameCount++;
        }
}
       
               private enum State 
               {
               ENTER_WAIT_ROOM, 
               WAIT_UNTIL_GAME, 
               LEAVE_RESPAWN, 
               GRAB_BARRICADES, 
               SLEEP
	};
                
        private State getState() 
        {
            /**
             * Enter portal
             */
              if (inArea(Areas.castlewarsLobby.area())) 
              {      
                   saradominTeam = false;
                   zamorakTeam = false;
                   log("In Castle Wars lobby Area");
                   return State.ENTER_WAIT_ROOM;
               }
              
              /**
               * Wait until next game
               */
              if (inArea(Areas.saradominWaitingRoom.area())) 
                  {
                      saradominTeam = true;
                      log("In Saradomin Waiting room");
                      return State.WAIT_UNTIL_GAME;
                  }
              else 
              {
              if (inArea(Areas.zamorakWaitingRoom.area())) 
              {
                  zamorakTeam = true;
                  log("In Zamorak Waiting room");
                  return State.WAIT_UNTIL_GAME;
              } 
                  
                  /**
                   * Leave respawn room
                   */
                  WidgetChild saradominRespawnRoomWidget = getWidgets().getWidgetChild(58, 24);
                  WidgetChild zamorakRespawnRoomWidget = getWidgets().getWidgetChild(59, 24);
                  if (saradominRespawnRoomWidget != null && saradominRespawnRoomWidget.getText().contains("minutes to leave the respawn room.")
                     || zamorakRespawnRoomWidget != null && zamorakRespawnRoomWidget.getText().contains("minutes to leave the respawn room.")) 
                  {
                      return State.LEAVE_RESPAWN;
                  }
          /**
           * Grab Barricades if in main floor else walk to there
           */
          if (!getInventory().contains(Item -> Item.getName().equals("Barricade"))) {
              if (inArea(Areas.saradominMainFloor.area()) || inArea(Areas.zamorakMainFloor.area()))
             return State.GRAB_BARRICADES;
          } else {
          if (saradominTeam) {
          getWalking().walk(Areas.saradominMainFloor.area().getRandomTile());
          } else {
          if (zamorakTeam) {
              getWalking().walk(Areas.zamorakMainFloor.area().getRandomTile());
              }}}
         }
            return State.SLEEP;
        }
        
	@Override
	public int onLoop() 
        {
            switch (getState()) 
            {
                case ENTER_WAIT_ROOM:
                    log("State = ENTER_WAIT_ROOM");
                    guthixPortal = getGameObjects().closest(gameObject -> gameObject != null && gameObject.getName().equals("Guthix Portal") && gameObject.hasAction("Enter"));
                    guthixPortal.interact();
                    sleepUntil(() -> inArea(Areas.zamorakWaitingRoom.area()) || inArea(Areas.saradominWaitingRoom.area()), 7000);
                break;
                
                case WAIT_UNTIL_GAME:
                    log("State = WAIT");
                     sleep(Calculations.random(150000, 199000));
                     getWalking().walk(getLocalPlayer().getTile().getRandomizedTile());
                break;
                
                case LEAVE_RESPAWN:
                energyBarrier = getGameObjects().closest(gameObject -> gameObject != null && gameObject.getName().equals("Energy Barrier") && gameObject.hasAction("Pass"));
                energyBarrier.interact();
                sleepUntil(() -> inArea(Areas.zamorakFirstFloor.area()) || inArea(Areas.saradominFirstFloor.area()), 3000);
                Ladder = getGameObjects().closest(gameObject -> gameObject != null && gameObject.getName().equals("Ladder") && gameObject.hasAction("Climb-down"));
                Ladder.interact();
                 sleepUntil(() -> inArea(Areas.zamorakMainFloor.area()) || inArea(Areas.saradominMainFloor.area()), 3000);
                break;
                
                case GRAB_BARRICADES:
                break;
                case SLEEP:
                    log("State = SLEEP");
                     sleep(Calculations.random(977, 1477));
                break;
        }
        return Calculations.random(950, 1050);
        }
        
@Override
	public void onPaint(Graphics g){
            g.setColor(Color.cyan);
			g.drawString("Runtime: " + timer.formatTime(), 10, 35);
                        //g.drawString("Games exp (p/h): " + getSkillTracker().getGainedExperience(Skill.FISHING) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.FISHING) + ")", 10, 65); //65
                                            
                       
}}


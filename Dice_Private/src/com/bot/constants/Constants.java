package com.bot.constants;

import org.dreambot.api.methods.map.Area;
import org.dreambot.api.methods.map.Tile;

/**
 *
 * @author T7emon, Tear x, Null x, T7x
 */
public class Constants {

    /*
    * Settings
   */
         private int minBetAmount = 100000; //The minimum bet amount coins 100k
         private int maxBetAmount = 100000000; //The maximum bet amount coins 100M
         private int chanceAmount = 55; //Chance amount for player to win. Above this number is payout x2 
         private int randomInt = 100; //Random number 100 for normal, 55 for rigged roll
         private boolean hotRollEnabled = true; //Hot Roll enabled true or false
         private int hotRollNumber = 88; //Hot Roll number for payout x3
         private String[] moderators = {"tunnellord", "shawtyblitz", "shawtyblits", "149122089147", "hiren"}; //Moderators nearby
         
         public boolean loggerEnabled = true; //Log trades in file, See com.bot.utils.Logger.java
         
         public Area botArea = Locations.Areas.castleWarsArea.area(); //The area where the bot is running at
         public Tile centerTile = Locations.centerTile.castleWarsCenterTile.tile(); //The tile in the botArea where the bot should always return to
         
         /*
         * Getters & Setters
       */
         /*
         * minBetAmount
        */
      public void setMinBetAmount(int minBetAmount) {
          this.minBetAmount = minBetAmount;
      }
      public int minBetAmount() {
          return minBetAmount;
    } 
      
     /*
     * minBetAmount as String
    */
      public String minBetAmountStr() {
      String coinsAmountStr = minBetAmount+"";
      if (minBetAmount >= 1000 && minBetAmount < 1000000) {
         coinsAmountStr = minBetAmount / 1000 + "K";
      } else {
          if (minBetAmount >= 1000000) {
          coinsAmountStr = minBetAmount / 1000000 + "M";
      }
      } 
      return coinsAmountStr;
}
      
         /*
         * maxBetAmount;
        */
      public void setMaxBetAmount(int maxBetAmount) {
          this.maxBetAmount = maxBetAmount;
      }
      public int maxBetAmount() {
          return maxBetAmount;
    }
      
     /*
     * maxBetAmount as String
    */
      public String maxBetAmountStr() {
      String coinsAmountStr = maxBetAmount+"";
      if (maxBetAmount >= 1000 && maxBetAmount < 1000000) {
         coinsAmountStr = maxBetAmount / 1000 + "K";
      } else {
          if (maxBetAmount >= 1000000) {
          coinsAmountStr = maxBetAmount / 1000000 + "M";
      }
      } 
      return coinsAmountStr;
}
       /*
       * chanceAmount;
      */
      public int chanceAmount() {
       return chanceAmount;
}
       /*
       * randomInt;
      */
      public int randomInt() {
       return randomInt;
}
      /*
      * hotRollEnabled;
     */
      public void setHotRollEnabled(boolean hotRollEnabled) {
          this.hotRollEnabled = hotRollEnabled;
      }
     public boolean hotRollEnabled() {
      return hotRollEnabled;
}
     /*
     * hotRollNumber;
    */
     public void setHotRollNumber(int hotRollNumber) {
         this.hotRollNumber = hotRollNumber;
     }
     public int hotRollNumber() {
         return hotRollNumber;
     }
     
     /*
     * moderators;
    */
     public String[] moderators() {
         return moderators;
     }
         
}

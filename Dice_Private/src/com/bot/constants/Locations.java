package com.bot.constants;

import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.map.Area;
/**
 *
 * @author T7emon, Tear x, Null x, T7x
 */
public class Locations {
    /*
    * @TODO Support multiple locations #UNFINISHED
   */
    
     /*
    * The area enum
    */
    public enum Areas {
      castleWarsArea(new Area(2438, 3082, 2446, 3097, 0));
      
      /*
      * area variable
      */
     private Area area;
     
     /*
     * The area setter.
     */
      private Areas(Area area) {
          this.area = area;
      }
      /*
      * The area getter.
      */
      public Area area() {
          return area;
      }
    }
    /*
    * CenterTiles
    */
    /*
    * The Tile enum
    */
    public enum centerTile {
        castleWarsCenterTile(new Tile(2441, 3087, 0));
       /*
        * tile variable
        */
            private Tile tile;
      
    /*
     * tile setter
     */
    private centerTile(Tile tile) {
        this.tile = tile;
    }
    /*
    * tile getter
    */
    public Tile tile() {
        return tile;
    }}}

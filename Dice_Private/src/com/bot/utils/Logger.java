package com.bot.utils;

import com.bot.constants.Constants;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

/**
 *
 * @author T7emon
 */
public class Logger {
    /*
    * @TODO, Log properly #UNFINISHED
    */
     
         private static final File DESKTOP_DIRECTORY = new File(System.getProperty("user.home"), "Desktop");
         private static final File LOG_DIRECTORY = new File(DESKTOP_DIRECTORY, "Trades"); //Downloads from multiple urls
         
                private static String getCurrentDateString() {
	        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	        return dateFormat.format(new Date());
	    }
                
          private static final File LOG_FILE = new File(LOG_DIRECTORY, "Trades_"+getCurrentDateString()+".log");
         private static  BufferedWriter Writer;
        
        public static void Log(String Message) {
            Constants Settings = new Constants();
            if (Settings.loggerEnabled) {
             try {
                 if (!LOG_DIRECTORY.exists()) {
                     LOG_DIRECTORY.mkdir();
                 }
         /*
         * File writer
         */
            Writer = new BufferedWriter(new FileWriter(""));
            Writer.append(Message + "\n");
             }  catch (IOException ex) {
                    java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
                }  finally {
                 try {
                     Writer.flush();
                     Writer.close();
                 } catch (IOException ex) {
                     java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
                 }
             }}}}

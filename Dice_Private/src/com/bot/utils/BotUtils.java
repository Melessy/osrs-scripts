/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.utils;

/**
 *
 * @author T7emon
 */
public class BotUtils {
             
     /*
     * Convert String coins e.g 100k to Coins integer 100000
    */
      public static int convertCoinsStrToInt(String s) {
      int coinsAmountInt = 0;
      if (s.contains("K")) {
         coinsAmountInt = Integer.parseInt(s.trim().toLowerCase().replaceAll("k", "")) ;
         coinsAmountInt = coinsAmountInt * 1000;
      } else {
          if (s.contains("M")) {
              coinsAmountInt = Integer.parseInt(s.trim().toLowerCase().replaceAll("m", "")) ;
              coinsAmountInt = coinsAmountInt * 1000000;
          }
      }
      return coinsAmountInt;
}}

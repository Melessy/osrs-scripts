package magic_private.superheater;

import java.awt.Graphics;
import org.dreambot.api.methods.Calculations;
import static org.dreambot.api.methods.MethodProvider.log;
import static org.dreambot.api.methods.MethodProvider.sleepUntil;
import org.dreambot.api.methods.magic.Normal;
import org.dreambot.api.methods.skills.Skill;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.script.Category;
import org.dreambot.api.utilities.Timer;
import org.dreambot.api.wrappers.items.Item;

@ScriptManifest(author = "T7emon", 
        name = "Magic_Bot", version = 1.0, 
        description = "Superheat iron ore", 
        category = Category.MAGIC)

public class Main extends AbstractScript 
{
    private Timer timer;
    private boolean superheat = false;
    private boolean alching = false;
    private boolean banking;
    
            public void init() 
            {
               timer = new Timer();
               getSkillTracker().start(Skill.MAGIC);
               getSkillTracker().start(Skill.SMITHING);
               log("Initialized");
            
        }

    @Override
	public void onStart() 
        {
            init();
		log("Welcome to Magic Bot by T7emon.");
		log("If you experience any issues while running this script please report them to me on the forums.");
		log("Enjoy the script, gain some Magic levels!.");
        }
        
    private void bank() 
    {
          banking = true;
          if (getBank().openClosest()) 
          {
          //GameObject bank = getGameObjects().closest(gameObject -> gameObject != null && gameObject.hasAction("Bank"));
          //bank.interact();
          sleepUntil(() -> getBank().isOpen(), 5000);
          sleep(Calculations.random(980, 1030));
          }
          if (getBank().isOpen() && !getBank().contains(Constants.iron_ore)) 
          {
              log("Ran out of Ores");
              this.stop();
          }
          getBank().depositAllExcept(Constants.nature_rune);
          sleepUntil(() -> getInventory().emptySlotCount() >= 26, 3000);
          getBank().withdraw(Constants.iron_ore, 27);
          sleepUntil(() -> getInventory().count(Constants.iron_ore) >= 27, 1500);
          getBank().close();
          banking = false;
}
        
        
	@Override
	public int onLoop() 
        {
            superheat = true;
            alching = false;
            
            /**
             * Alching
             */
            if (alching) 
            {
                    getMagic().castSpell(Normal.HIGH_LEVEL_ALCHEMY);
                    Item item = getInventory().get("Steel platelegs");
                    item.interact();
                    sleepUntil(() -> !getLocalPlayer().isAnimating(),3000);
                }
                    /**
                     * Superheat
                     */
		     if (superheat) 
                     {
                         
                     if (!getInventory().contains(Constants.nature_rune)) // Stop if there are no nature runes in inventory
                     {
                       this.stop();
                     }

                     if (!getInventory().contains(Constants.iron_ore) 
                     && getMagic().isSpellSelected()) // Click mouse when spell is selected and out of iron ores
                     {
                         getMouse().click();
                     }
                     else {
                     if (!getInventory().contains(Constants.iron_ore)) {
                     bank();
                     }}
                     
                     if (!getMagic().isSpellSelected() 
                     && getInventory().contains(Constants.iron_ore) 
                     && !banking) 
                     {
                    getMagic().castSpell(Normal.SUPERHEAT_ITEM);
                    Item item = getInventory().get(Constants.iron_ore);
                      if (getMagic().isSpellSelected()) 
                      {
                          item.interact();
                      }}
                     }
		return Calculations.random(0, 0);
        }
        
        @Override
	public void onPaint(Graphics g) 
        {
			g.drawString("Runtime: " + timer.formatTime(), 10, 35);
                        g.drawString("Magic exp (p/h): " + getSkillTracker().getGainedExperience(Skill.MAGIC) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.MAGIC) + ")", 10, 65);
                        g.drawString("Smithing exp (p/h): " + getSkillTracker().getGainedExperience(Skill.SMITHING) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.SMITHING) + ")", 10, 75);
                                            
                       
}}

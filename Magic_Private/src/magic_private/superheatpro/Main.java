/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package magic_private.superheatpro;

import java.awt.Color;
import java.awt.Graphics;
import org.dreambot.api.methods.Calculations;
import static org.dreambot.api.methods.MethodProvider.log;
import static org.dreambot.api.methods.MethodProvider.sleepUntil;
import org.dreambot.api.methods.magic.Normal;
import org.dreambot.api.methods.skills.Skill;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.script.Category;
import org.dreambot.api.utilities.Timer;
import org.dreambot.api.wrappers.items.Item;

@ScriptManifest(
        author = "T7emon", 
        name = "Superheat pro", version = 1.0, 
        description = "Superheat iron ore", 
        category = Category.MAGIC)

public class Main extends AbstractScript 
{
    /**
     * Variables
     */
    private Timer timer;
    private int superHeatsAmount;
    
    /**
     * Initialize
     */
            public void init() 
            {
               timer = new Timer();
               getSkillTracker().start(Skill.MAGIC);
               getSkillTracker().start(Skill.SMITHING);
               log("Initialized");
            
        }

            /**
             * Start
             */
        @Override
	public void onStart() 
        {
               init();
	       log("Welcome to Superheat Bot by T7emon, Tear x, T7x, Melessy.");
               log("To use this script, you need Nature runes in your inventory and Iron ores in bank.");
        }
    
   /**
    * State enumeration
    */
               private enum State 
               {
               BANK,
               SUPERHEAT
	};
        
    /**
     * State getter
     */           
        private State getState() 
        {
            
        /**
         * Stop if there are no nature runes in inventory
         */    
        if (!getInventory().contains(Constants.nature_rune))
        {
        this.stop();
        }
        
        /**
         * Click mouse when spell is selected and out of iron ores
         */
          if (!getInventory().contains(Constants.iron_ore) 
          && getMagic().isSpellSelected())
         {
         getMouse().click();
         }
        
          /**
           * Bank state
           */
        if (!getInventory().contains(Constants.iron_ore) 
        && !getMagic().isSpellSelected()) 
        {
        return State.BANK;
        }
        
        /**
         * Superheat state
         */
        return State.SUPERHEAT;
            
        }
        
       /**
        * The mighty loop
        * @return sleep
        */
	@Override
	public int onLoop() 
        {
          switch (getState()) 
          { 
              
          /**
           * Handle bank
           */    
          case BANK:
          if (getBank().openClosest()) 
          {
          sleepUntil(() -> getBank().isOpen(), 5000);
          sleep(Calculations.random(980, 1030));
          }
          if (getBank().isOpen() && !getBank().contains(Constants.iron_ore)) 
          {
              log("Ran out of Ores");
              this.stop();
          }
          getBank().depositAllExcept(Constants.nature_rune);
          sleepUntil(() -> getInventory().emptySlotCount() >= 26, 3000);
          getBank().withdraw(Constants.iron_ore, 27);
          sleepUntil(() -> getInventory().count(Constants.iron_ore) >= 27, 1500);
          getBank().close();
          break;
          
          /**
           * Handle superheat
           */
              case SUPERHEAT:
              getMagic().castSpell(Normal.SUPERHEAT_ITEM);
              Item item = getInventory().get(Constants.iron_ore);
              if (getMagic().isSpellSelected()) 
              {
              item.interact();
              superHeatsAmount++;
              }
              break;
              }
          
              return Calculations.random(0, 0);
        }
        
        /**
         * Paint
         * @param g
         */
        @Override
	public void onPaint(Graphics g) 
        {
        g.setColor(Color.CYAN);
	g.drawString("Runtime: " + timer.formatTime(), 10, 35);
        g.drawString("State: " + getState(), 10, 60);
        g.drawString("Superheats (p/h): " + superHeatsAmount + "(" + timer.getHourlyRate(superHeatsAmount) + ")", 10, 75);
        g.drawString("Magic exp (p/h): " + getSkillTracker().getGainedExperience(Skill.MAGIC) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.MAGIC) + ")", 10, 90);
        g.drawString("Smithing exp (p/h): " + getSkillTracker().getGainedExperience(Skill.SMITHING) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.SMITHING) + ")", 10, 105);                                    
}
}

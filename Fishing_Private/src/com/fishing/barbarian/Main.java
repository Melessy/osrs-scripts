/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.fishing.barbarian;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.skills.Skill;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.utilities.Timer;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.api.wrappers.widgets.message.Message;

@ScriptManifest(
        author = "T7emon", 
        name = "Fishing_Private", 
        version = 1.0, 
        description = "Barbarian Fishing", 
        category = Category.FISHING)

public class Main extends AbstractScript 
{
    
    private Timer timer;
    NPC Fishing_spot;
    private int fish_count = 0;
    
            public void init() 
            {
               timer = new Timer();
               getSkillTracker().start(Skill.FISHING);
               getSkillTracker().start(Skill.AGILITY);
               getSkillTracker().start(Skill.STRENGTH);
               log("Initialized");
            
        }

    @Override
	public void onStart() 
        {
            init();
		log("Welcome to Barbarian Fishing Bot by T7emon.");
		log("If you experience any issues while running this script please report them to me on the forums.");
		log("Enjoy the script, gain some Fishing levels!.");
        }
        
            @Override
public void onMessage(Message msg) 
{
	if (msg.getMessage().contains("You catch a leaping trout.") 
                || msg.getMessage().contains("You catch a leaping salmon.") 
                || msg.getMessage().contains("You catch a leaping sturgeon.")) 
        {
           fish_count++;
        }
}
       
        	private enum State 
                {
               FISH, DROP
	};
                
        private State getState() 
        {
            
            if (!getInventory().contains(Constants.Barbarian_rod) || !getInventory().contains(Constants.Feathers)) 
            {
                log("You need a Barbarian rod & Feathers to use this script!");
                this.stop();
            }
            
              if (getDialogues().inDialogue()) 
              {
                     getDialogues().clickContinue();
                     return State.FISH;
               }
              if (getInventory().count(Constants.Leaping_trout) > new Random().nextInt(6 + 1) + 10 
                || getInventory().count(Constants.Leaping_salmon) > new Random().nextInt(6 + 1) + 10 
                || getInventory().count(Constants.Leaping_sturgeon) > new Random().nextInt(6 + 1) + 10 
                || getInventory().isFull()) 
              {
              return State.DROP;
              }
            return State.FISH;
        }
        
	@Override
	public int onLoop() 
        {
            switch (getState()) 
            {
                case FISH:
                     Fishing_spot = getNpcs().closest("Fishing spot");
                     if (!getLocalPlayer().isInteracting(Fishing_spot) && Fishing_spot.interactForceRight("Use-rod")) 
                     {
                        sleepUntil(()-> !getLocalPlayer().isInteracting(Fishing_spot), 240000);
                    }
                break;
                case DROP:
                   // getInventory().dropAllExcept(Constants.Barbarian_rod, Constants.Feathers);
                     getInventory().dropAll(Item -> Item != null && Item.getName().contains("Leaping"));
                     sleepUntil(()-> !getInventory().contains(Constants.Leaping_trout) 
                             || !getInventory().contains(Constants.Leaping_salmon) 
                             || !getInventory().contains(Constants.Leaping_sturgeon) , 240000);
                     sleep(Calculations.random(977, 1477));
                     getMouse().click(Fishing_spot);
                break;
        }
        return Calculations.random(950, 1050);
        }
        
@Override
	public void onPaint(Graphics g) 
        {
            g.setColor(Color.cyan);
			g.drawString("Runtime: " + timer.formatTime(), 10, 35);
                        g.drawString("Fishing exp (p/h): " + getSkillTracker().getGainedExperience(Skill.FISHING) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.FISHING) + ")", 10, 65);
                        g.drawString("Agility exp (p/h): " + getSkillTracker().getGainedExperience(Skill.AGILITY) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.AGILITY) + ")", 10, 80);
                        g.drawString("Strength exp (p/h): " + getSkillTracker().getGainedExperience(Skill.STRENGTH) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.STRENGTH) + ")", 10, 95);
                        g.drawString("Fish gained (p/h): " + fish_count + "(" + timer.getHourlyRate(fish_count) + ")", 10, 110);                                   
}}


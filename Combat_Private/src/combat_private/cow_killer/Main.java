/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package combat_private.cow_killer;


import java.awt.Color;
import java.awt.Graphics;
import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.skills.Skill;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.utilities.Timer;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.api.wrappers.widgets.message.Message;

@ScriptManifest(author = "T7emon", 
        name = "Cow_Killer_Private", 
        version = 1.0, 
        description = "Kill Cows", 
        category = Category.COMBAT)

public class Main extends AbstractScript 
{
    
    private Timer timer;
    private int kills = 0;
    
            public void init() 
            {
               timer = new Timer();
               getSkillTracker().start(Skill.HITPOINTS);
               getSkillTracker().start(Skill.ATTACK);
               getSkillTracker().start(Skill.STRENGTH);
               getSkillTracker().start(Skill.DEFENCE);
               getSkillTracker().start(Skill.RANGED);
               getSkillTracker().start(Skill.MAGIC);
               log("Initialized");
        }

    @Override
	public void onStart() 
        {
            init();
		log("Welcome to Cow Killer Bot by T7emon.");
        }
        
            @Override
public void onMessage(Message msg) 
{
	if (msg.getMessage().contains("There is no ammo left in your quiver.")) 
        {
           log("There is no ammo left in your quiver.");
           this.stop();
        }
}
       
        	private enum State 
                {
               BANK, FIGHT, EAT
	};
                
        private State getState() 
        {
            
            if (!getInventory().contains(Constants.food) && Constants.food_enabled) 
            {
                log("You need food We dont bank just stop");
                this.stop();
            }
            
              if (getDialogues().inDialogue()) 
              {
                     getDialogues().clickContinue();
                     return State.FIGHT;
               }
              
              NPC npc = getNpcs().closest(NPC -> NPC.getName().contains(Constants.NPC));         
              if (npc != null && npc.isOnScreen() 
                      && !npc.isInCombat() 
                      && !getLocalPlayer().isInCombat() 
                      && !npc.isInteractedWith() 
                      && !npc.isInteracting(getLocalPlayer())) 
              {
                  return State.FIGHT;
              }
              
              if (getLocalPlayer().getHealthPercent() < 40) 
              {
                  return State.EAT;
              }
            
            return State.FIGHT;
        }
        
	@Override
	public int onLoop() 
        {
            switch (getState()) 
            {
                
                case BANK:
                // @TODO fix banking? 
                break;
                
                case FIGHT:
                NPC npc = getNpcs().closest(NPC -> NPC.getName().contains(Constants.NPC));
                if (npc.interact()) 
                {
                    sleepUntil(() -> !npc.isInteractedWith() && !npc.isDrawMinimapDot(), 15000); //!npc.drawnminimap
                    kills++;
                }
                break;
                case EAT:
                    getInventory().getRandom(Constants.food).interact();
                break;
        }
        return Calculations.random(100, 300);
        }
        
        @Override
	public void onPaint(Graphics g) 
        {
            g.setColor(Color.LIGHT_GRAY);
			g.drawString("Runtime: " + timer.formatTime(), 10, 35);
                        g.drawString("Kills: " + kills, 10, 50);
                        g.drawString("Hitpoints (p/h): " + getSkillTracker().getGainedExperience(Skill.HITPOINTS) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.HITPOINTS) + ")", 10, 65);
                        g.drawString("Attack exp (p/h): " + getSkillTracker().getGainedExperience(Skill.ATTACK) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.ATTACK) + ")", 10, 80);
                        g.drawString("Strength exp (p/h): " + getSkillTracker().getGainedExperience(Skill.STRENGTH) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.STRENGTH) + ")", 10, 95);
                        g.drawString("Defence exp (p/h): " + getSkillTracker().getGainedExperience(Skill.DEFENCE) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.DEFENCE) + ")", 10, 110);
                        g.drawString("Ranged exp (p/h): " + getSkillTracker().getGainedExperience(Skill.RANGED) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.RANGED) + ")", 10, 125);
                        g.drawString("Magic exp (p/h): " + getSkillTracker().getGainedExperience(Skill.MAGIC) + "(" + getSkillTracker().getGainedExperiencePerHour(Skill.MAGIC) + ")", 10, 140);
        }}

